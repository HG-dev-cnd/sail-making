
This repository contains a series of Freecad macros for processing wing sections of the Avian p7 and R13 
hang-glider wings into dxf of flat pieces of fabric with pen marks that can be further edited and sent to the 
laser cutter.

Finished freecad files (complete with airframe, wires and sail cuts) are in the examples directory.

Set the freecad user macros directory to the freecad_macros directory.  
Note that most of the code functions have been moved into the p7modules directory to shorten the 
code in each of these macros.


### Defining the wing shape in 3D

#### Changing the planform (XY extent) of the wing

**SprShttoplanform.py** - This fetches the Spreadsheet (labelled as Configurations) and uses the lists in the cells marked 
secs, chords, sweeps and DSdists to generate the Sail.planformsketch of the XY extent of the wing.  
(This is approximate because it does not take account of the twist.)

**planformtoSprSht.py** - This takes any changes to the node positions in the (flattened) planformsketch and writes them back into the 
(Configurations) Spreadsheet.

**SprShttoWingSections.py** - Reads the aerofoil wing cross sections defined in 
`data/aerofoils/\[ui1720reflex.csv,2R15section.csv,tipsection.csv]` as polygons, 
and interpolates them against the positions, twists and sizes defined in the (Configurations) Spreadsheet.  
Output is `WingSections` folder containing `wingsection_\[0-7]` and a `doublesurfaceline` and `loftedwingsurface` 
for reference.

`WingSections` are the basis for the parametrization of the wing urange and vrange evalutation functions found in 
p7modules/p7wingeval

The wingsection `Vparameter_wingsect` is used for the parametrization.  It is originally set to be the same as 
section5 of the WingSections, but does not get deleted the next time it is regenerated so that the parametrizations 
get preserved.

**p7csvtoWingSections.py** - Reads the p7 aerofoil sections that are saved in `examples/P7-220527-XYZ geometry.csv` and 
projects them into the WingSections the same way that is done by `SprShttoWingSections.py` so all subsequent 
processing can carry on from this stage.  The `Vparameter_wingsect` value is done from section7.  
This makes the scripts incompatible so you can only have one set of WingSections in the FreeCAD project

**WingSectonstoplanformREFsections.py** - NOTE: You must have opened Part Design or Sketcher Workbench before calling this.
Constructs the true `planformREF` sketch from the `wingeval`.  
Creates the sketches `cutouts_upper`, `cutouts_lower`, `penmarks_upper` and `penmarks_lower` if they do 
not already exist.  This is so any cutouts made on the previous shaped wing can be reused.  
Unfortunately, any externally referenced geometry pulled from `planformREF` is lost when the 
`planformREF` sketch is regenerated.  However, you can move the cutouts and penmarks to match them by hand.

**cutoutstocutlinesketchREF.py** - Projects `cutouts_upper` and `cutouts_lower` into `cutlinesketchREF` for using as a basis 
for aligning the cuts and panels that are in `cutlinesketch`.  

If `cutlinesketch` does not exist, then this projects creates the UV projection rectangle and 
projects the cutouts directly into it, attempting to include the coincident constraints.  
This won't work for tangential constraints that aren't seen as coincident, 
so we might need to go by coincident positions.

It is not possible to project the the cutouts directly 
into a `cutlinesketch` that already exists without messing up any panels that are drawn into there.  

This macro also projects `penmarks_upper` and `penmarks_lower` into `penmarkssketch`. 
Since `penmarkssketch` is not intended to be edited, 
it can be overwritten like this, unlike `cutlinesketch`. 

-- You can change the colour and height of the penmark sketches 
to distinguish its contents from the cutlines by editing values in the 
`Data` or `View` tab of the sketch respectively.  These are not reset when 
the script is rerun because the sketch object itself is cleared and reused.

-- NOTE: When drawing in panels to cut the use only coincident constraints to create panels. DO NOT use 'Point on line' type constraints as these will not be recognised as separate panels inthe subsequent macros. Basically, use polylines for everything.

**cutlinesketchto3Dwires.py** - This creates the preview groups `Scutlinewires` and `Spenmarkwires` from `cutlinesketch` and 
`penmarkssketch` for use in planning and verification that the cutouts and penlines will 
be in the places that are expected without needing to regenerate all the panels.




**p7showmeshdistortions** - sets the facet colours for the corresponding meshes in STriangulations and Sflattened.
Requires you to right click on each surface in the tree view and select "Display colors" to see it.  A pain!

**p7stretchcurvesonpatches** - resamples the UVpolygons at 1cm intervals (in real space) and projects them into the flat area 
in the folder SStretch.  Also creates SStretch lines with a sideways displacement according 
to how out of line to 1cm the sample rates are, multiplied by uvpolysampleSideErrorFactor


**p7drawlinesonpatches** - This draws the overlaps from each panel onto the next panel, as well as the other 
things like fold lines for pen cutting






### Old stuff not yet upgraded -SO IF YOU USE BEYOND THIS POINT YOU WILL NEED TO OPEN THE RELEVANT MACROS IN NOTEPAD AND EDIT THEM TO IDENTIFY THE PANEL LOCATIONS.

#### Building the flattened offset patches with penlines


**p7cutlinestouvpolygons.py** - Creates `UVPolygons` containing polygons of each of the areas that the 
`cutlinessketch` divides into.  Points need to be joined with the Coincident constraint to be 
considered a continuation of the polygon.  Polygons are given names according to a lookup table.


**p7uvpolygonsfoldedoffsets.py** - Extends polygons that are on the trailing edges by 6 or 18mm so that 
when the whole shape is offset by 6mm they are at 12mm and 24mm to allow the fabric to be folded over 
and sewn.  The outputs are UVPolygonOffsets and UVPolygonsFoldlines (though if they don't exist, UVPolygons 
will be used by in a subsequent stage)

**R13lowertrailingedgesewline.py** - Creates UVLWsewline for where the lower surface meets the upper surface 
in UV space. 

**p7uvpolygonstouvmeshes.py** - offsets by 6mm and triangulates each polygon in UV space into UVTriangulations

**p7uvmeshestosurfaces.py** - projects each triangulated mesh in UV space into the wing surface 
by evaluating the function WingEval.SEval().  These go into STriangulations.  Then flatmesh.FaceUnwrapper()
is called on each surface and the flattened areas are put into SFlattened

**p7flattenpatchestojson_forviewing.py** - outputs STriangulations and SFlattened lists into the file ../p7test.json
which can be previewed using the Jupyter notebook p7flattenpatches_viewer to see the distortion and flatness 
of the flattened patches

**p7drawlinesonpatches.py** - projects the outlines of all the adjacent patches into SPencil groups of each patch, 
as well as projecting the "batten detail TSR.dxf" onto the trailing edge of each section

**p7drawpatchestodxf.py** - Outputs the file "../p7test.dxf" from the SFlattened outlines and SPencil lines


* For sewing machines, consider https://www.castlesewing.co.uk/product/long-arm-zig-zag-walking-foot-zig-zag/

Missing origin can be set from python:

doc = App.ActiveDocument
doc.Body.Origin = doc.addObject("App::Origin", "Origin")

* https://forum.freecadweb.org/viewtopic.php?p=396091&sid=a55664f32262f3c8db20f884d48126c2#p396091

