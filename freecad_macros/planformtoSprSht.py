import math
import json

doc = App.ActiveDocument
ss = doc.Spreadsheet
#print(doc.planformsketch.Geometry)
geo = doc.planformsketch.Geometry
l=int(len(geo)/4)
stats = geo[:l+1]
LEs=[]
TEs=[]
DSs=[]
i=0
for ls in geo[l+1:]:
	if i%3 ==0:
		LEs.append(ls)
	elif i%3 ==1:
		TEs.append(ls)
	else:
		DSs.append(ls)
	i+=1

secs=[]
chords=[]
sweeps=[]
double_surf_dist=[]

for li in stats:
	secs.append(li.StartPoint.x)
	chords.append(li.StartPoint.y - li.EndPoint.y)

for li in LEs:
	#print(li)
	dy = li.StartPoint.y-li.EndPoint.y
	dx = li.EndPoint.x-li.StartPoint.x
	sweeps.append(math.degrees(math.atan(dy/dx)))

for i in range(len(LEs)):
	double_surf_dist.append( LEs[i].StartPoint.y - DSs[i].StartPoint.y)
double_surf_dist.append(LEs[-1].EndPoint.y - DSs[-1].EndPoint.y)
print('secs',secs)
print('chords',chords)
print('sweeps',sweeps)
print('double_surf_dist',double_surf_dist)

ss.set("secs", json.dumps(secs))
ss.set("chords", json.dumps(chords))
ss.set("sweeps", json.dumps(sweeps))
ss.set("double_surf_dist", json.dumps(double_surf_dist))

doc.recompute()
