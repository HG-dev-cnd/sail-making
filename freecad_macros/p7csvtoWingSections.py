# -*- coding: utf-8 -*-
# Macro to make project7wing as list of wires and single bspline surface

import FreeCAD as App
import Draft, Part, Mesh
import DraftGeomUtils
import math, os, csv
from FreeCAD import Vector, Rotation

sys.path.append(os.path.split(__file__)[0])

p7csvfile = "/home/timbo/Dropbox/Avian/Technical/Development/project7/Hang_glider/sail_model/P7-230324-XYZ.csv"
p7csvfile = os.path.join(os.path.dirname(os.path.abspath(__file__)), p7csvfile)

doc = App.ActiveDocument

from p7modules.p7wingeval import getemptyfolder, createobjectingroup
wingsectionsgroup = getemptyfolder(doc, "WingSections")

Vparameter_wingsectionL = "Vparameter_wingsect"

Vparameter_wingsection = None
objs = doc.findObjects(Label=Vparameter_wingsectionL)
if objs:
	Vparameter_wingsection = doc.copyObject(objs[0])
	print("CnewVsection ", Vparameter_wingsection.Name, Vparameter_wingsection.Label)

def loadwinggeometry(fname):
	k = list(csv.reader(open(fname)))
	wingmeshuvudivisions = eval(k[0][-3])
	assert (wingmeshuvudivisions == len(k[0])/3-1), 'Section numbering incorrect'
	sections, zvals = [], []
	for i in range(0, (wingmeshuvudivisions*3)+2, 3):
		pts = [ ]
		z = float(k[2][i+1])
		for j in range(2, len(k)):
			assert (z == float(k[j][i+1]))
			pts.append(Vector(0.0, -float(k[j][i]), float(k[j][i+2])))
		zvals.append(z)
		sections.append(pts)
	assert(len(sections) == wingmeshuvudivisions+1)
	return sections, zvals
	

#Function to mesh between two aero-profiles
def foil_mesh(points1, points2, trianglepoints):
	trianglepoints = trianglepoints
	for i in range(len(points1)-1):
		ip = (i+1)
		trianglepoints.append(points1[i]);  trianglepoints.append(points2[i]);   trianglepoints.append(points2[ip]); 
		trianglepoints.append(points1[i]);  trianglepoints.append(points2[ip]);  trianglepoints.append(points1[ip]); 
	return trianglepoints


# Spreadsheet containing all the wingsections
sections, zvals = loadwinggeometry(p7csvfile)
print(zvals, len(sections))
trianglepoints = [ ]

Isection = 7

for station in range(len(zvals)):
	pts = sections[station]
	pts[-1] = pts[-1] - Vector(0,0,0.001)
	section = Draft.makeWire(pts, closed=False)
	section.Label = "wingsection_%d" % station
	section.adjustRelativeLinks(wingsectionsgroup)
	wingsectionsgroup.addObject(section)
	print("newsection ", section.Name, section.Label)
	pp = Vector(zvals[station], 0.0, 0.0)
	section.Placement = App.Placement(pp, App.Rotation(), App.Vector())

	if Vparameter_wingsection == None and station == Isection:
		Vparameter_wingsection = doc.copyObject(section)
		print("newVsection ", Vparameter_wingsection.Name, Vparameter_wingsection.Label)

	points = [p + pp  for p in pts]
	if station > 0:
		trianglepoints = foil_mesh(last_points, points, trianglepoints)
	last_points = points

fmesh = createobjectingroup(doc, wingsectionsgroup, "Mesh::Feature", "loftedwingsurface")
fmesh.Mesh = Mesh.Mesh(trianglepoints)

print(Vparameter_wingsection.Name, Vparameter_wingsection.Label)
Vparameter_wingsection.Label = Vparameter_wingsectionL
print(Vparameter_wingsection.Name, Vparameter_wingsection.Label)

Vparameter_wingsection.adjustRelativeLinks(wingsectionsgroup)
wingsectionsgroup.addObject(Vparameter_wingsection)

doc.recompute([wingsectionsgroup])
NVwingsecVerts = len(Vparameter_wingsection.Shape.OrderedVertexes)
for lsection in wingsectionsgroup.OutList:
	if lsection.Label.startswith("wingsection_"):
		NsecVerts = len(lsection.Shape.OrderedVertexes)
		assert NsecVerts == NVwingsecVerts, ("Error: wingsection has different number of nodes to Vparameter_wingsection", NsecVerts, NVwingsecVerts)

