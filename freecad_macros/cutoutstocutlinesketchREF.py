# -*- coding: utf-8 -*-

# Macro to generate the pencil lines onto each of the patches 
# based on the other outlines and on the batton patch design

import FreeCAD as App
import Draft, Part, Mesh
import DraftGeomUtils
import math, os, csv, sys, math
import numpy
from FreeCAD import Vector, Rotation

sys.path.append(os.path.split(__file__)[0])

from p7modules.barmesh.basicgeo import P2, P3, Partition1, Along, I1
from p7modules.p7wingeval import WingEval
from p7modules.p7wingeval import getemptyobject, createobjectingroup, getemptyobject, uvrectangle, makeclearedsketch

doc = App.ActiveDocument

wingeval = WingEval(doc, True)
urange, vrange, seval = wingeval.urange, wingeval.vrange, wingeval.seval

legsampleleng = 3.0
def projectprecut(parametricsketch, plansketch, bupperface):
	geometrysketchmap = { }
	for i, g in enumerate(plansketch.GeometryFacadeList):
		if not g.Construction:
			gg = g.Geometry
			num = int(math.ceil(gg.length()/legsampleleng) + 1)
			params = numpy.linspace(gg.FirstParameter, gg.LastParameter, num)
			qs = [ ]
			for a in params:
				p = gg.value(a)
				q = wingeval.inverse_seval(p.x, p.y, bupperface, tol=0.001)
				qs.append(Vector(q[0], q[1]))
			cbspline = Part.BSplineCurve()
			cbspline.approximate(qs, Parameters=params, DegMin=2, DegMax=2)
			print(cbspline)
			j = parametricsketch.addGeometry(cbspline, False)
			geometrysketchmap[i] = j
			
	for d in plansketch.Constraints:
		if d.IsActive and d.Type == "Coincident" and d.First in geometrysketchmap and d.Second in geometrysketchmap:
			if (d.FirstPos == 1 or d.FirstPos == 2) and (d.SecondPos == 1 or d.SecondPos == 2):
				parametricsketch.addConstraint(Sketcher.Constraint("Coincident", geometrysketchmap[d.First], d.FirstPos, geometrysketchmap[d.Second], d.SecondPos))

def uvrectangleN(doc, sketch, urange, vrange, asconstruction): 
	p00 = Vector(urange[0], vrange[0])
	p01 = Vector(urange[0], vrange[1])
	p10 = Vector(urange[1], vrange[0])
	p11 = Vector(urange[1], vrange[1])

	e0x = sketch.addGeometry(Part.LineSegment(p00, p01), asconstruction)
	e1x = sketch.addGeometry(Part.LineSegment(p10, p11), asconstruction)
	ex0 = sketch.addGeometry(Part.LineSegment(p00, p10), asconstruction)
	ex1 = sketch.addGeometry(Part.LineSegment(p01, p11), asconstruction)

	sketch.addConstraint(Sketcher.Constraint("Vertical", e0x))
	sketch.addConstraint(Sketcher.Constraint("Vertical", e1x))
	sketch.addConstraint(Sketcher.Constraint("Horizontal", ex0))
	sketch.addConstraint(Sketcher.Constraint("Horizontal", ex1))

	sketch.addConstraint(Sketcher.Constraint("Coincident", e0x, 1, ex0, 1))
	sketch.addConstraint(Sketcher.Constraint("Coincident", e0x, 2, ex1, 1))
	sketch.addConstraint(Sketcher.Constraint("Coincident", ex0, 2, e1x, 1))
	sketch.addConstraint(Sketcher.Constraint("Coincident", e1x, 2, ex1, 2))

	sketch.addConstraint(Sketcher.Constraint('DistanceX', e0x, 1, urange[0])) 
	sketch.addConstraint(Sketcher.Constraint('DistanceY', e0x, 1, vrange[0])) 
	sketch.addConstraint(Sketcher.Constraint('DistanceX', e1x, 2, urange[1])) 
	sketch.addConstraint(Sketcher.Constraint('DistanceY', e1x, 2, vrange[1])) 

			
objs = doc.findObjects(Label="cutlinesketch")
if objs:
	cutlinesketch = objs[0]
	print("Find the constraints on the e0x etc lines and check or reset them")
	planformsketchREF = makeclearedsketch(doc, "planformsketchREF")
	cutlinesketchREF = makeclearedsketch(doc, "cutlinesketchREF")
	uvrectangleN(doc, cutlinesketchREF, urange, vrange, False)

else:
	cutlinesketch = makeclearedsketch(doc, "cutlinesketch")
	uvrectangleN(doc, cutlinesketch, urange, vrange, True)
	cutlinesketchREF = cutlinesketch 

penmarkssketch = makeclearedsketch(doc, "penmarkssketch")

cutouts_upper = doc.findObjects(Label="cutouts_upper")[0]
cutouts_lower = doc.findObjects(Label="cutouts_lower")[0]
penmarks_upper = doc.findObjects(Label="penmarks_upper")[0]
penmarks_lower = doc.findObjects(Label="penmarks_lower")[0]

projectprecut(cutlinesketchREF, cutouts_upper, True)
projectprecut(cutlinesketchREF, cutouts_lower, False)

projectprecut(penmarkssketch, penmarks_upper, True)
projectprecut(penmarkssketch, penmarks_lower, False)

for i in range(len(penmarkssketch.Constraints)):
	penmarkssketch.setVirtualSpace(i, True)
for i in range(len(cutlinesketchREF.Constraints)):
	cutlinesketchREF.setVirtualSpace(i, True)
