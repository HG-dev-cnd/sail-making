# -*- coding: utf-8 -*-
# Macro to make project7wing as list of wires and single bspline surface
import Draft, Part, Mesh
import DraftGeomUtils
import math, os, csv, sys
from FreeCAD import Vector, Rotation

sys.path.append(os.path.split(__file__)[0])
from p7modules.p7wingeval import WingEval
from p7modules.p7wingeval import getemptyobject, createobjectingroup, uvrectangle, makeclearedsketch

doc = App.ActiveDocument

wingeval = WingEval(doc, True)
urange, vrange, seval = wingeval.urange, wingeval.vrange, wingeval.seval
uvals, sections = wingeval.uvals, wingeval.sections

xvals = wingeval.xvals
leadingedgesY = [ seval(u, v).y  for u, v in zip(uvals, wingeval.leadingedgesV) ]
trailingedgesUpperY = [ seval(u, vrange[0]).y  for u in uvals ]
trailingedgesLowerY = [ seval(u, vrange[1]).y  for u in uvals ]

def createobjectifnotexist(doc, lname, objtype):
	if not doc.findObjects(Label=lname):
		doc.addObject(objtype, lname)

def yseqsegments(yedges):
	return [ planformsketchREF.addGeometry(Part.LineSegment(Vector(xvals[i], yedges[i]), Vector(xvals[i+1], yedges[i+1])), False)  \
			 for i in range(len(xvals)-1) ]

planformsketchREF = makeclearedsketch(doc, "planformsketchREF")

leadingedgesY = [ seval(u, v).y  for u, v in zip(uvals, wingeval.leadingedgesV) ]
trailingedgesUpperY = [ seval(u, vrange[0]).y  for u in uvals ]
trailingedgesLowerY = [ seval(u, vrange[1]).y  for u in uvals ]

SleadingEdges = yseqsegments(leadingedgesY)
StrailingUpperEdges = yseqsegments(trailingedgesUpperY)
StrailingLowerEdges = yseqsegments(trailingedgesLowerY)
Sleftedge = planformsketchREF.addGeometry(Part.LineSegment(Vector(xvals[0], leadingedgesY[0]), Vector(xvals[0], trailingedgesUpperY[0])), False)
Srightedge = planformsketchREF.addGeometry(Part.LineSegment(Vector(xvals[-1], leadingedgesY[-1]), Vector(xvals[-1], trailingedgesUpperY[-1])), False)

	
createobjectifnotexist(doc, "cutouts_upper", "Sketcher::SketchObject")
createobjectifnotexist(doc, "cutouts_lower", "Sketcher::SketchObject")
createobjectifnotexist(doc, "penmarks_upper", "Sketcher::SketchObject")
createobjectifnotexist(doc, "penmarks_lower", "Sketcher::SketchObject")


for i in range(len(planformsketchREF.Geometry)):
	l = planformsketchREF.Geometry[i]
	planformsketchREF.addConstraint(Sketcher.Constraint('DistanceX', i, 1, l.StartPoint.x)) 
	planformsketchREF.addConstraint(Sketcher.Constraint('DistanceY', i, 1, l.StartPoint.y)) 
	planformsketchREF.addConstraint(Sketcher.Constraint('DistanceX', i, 2, l.EndPoint.x)) 
	planformsketchREF.addConstraint(Sketcher.Constraint('DistanceY', i, 2, l.EndPoint.y)) 
for i in range(len(planformsketchREF.Constraints)):
	planformsketchREF.setVirtualSpace(i, True)




