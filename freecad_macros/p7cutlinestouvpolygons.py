# -*- coding: utf-8 -*-

# This macro is for generating a set of PatchUVPolygons (polygons in UV space)
# derived from the cutlinesketch diagram, which are to be used as the basis of 
# the flattening, offsetting and trimming 

import FreeCAD as App
import Draft, Part, Mesh
import DraftGeomUtils
import math, os, csv, sys
import numpy
from FreeCAD import Vector, Rotation

sys.path.append(os.path.split(__file__)[0])
from p7modules.p7wingeval import WingEval
from p7modules.p7wingeval import createobjectingroup, getemptyfolder, getemptyobject
from p7modules.barmesh.basicgeo import P2

doc = App.ActiveDocument
cutlinesketch = doc.cutlinesketch

R13type = doc.getObject("Group")
wingeval = WingEval(doc)

urange, vrange, seval = wingeval.urange, wingeval.vrange, wingeval.seval

print("R13 type offsets" if R13type else "P7 wing offsets")

print("Rangesss", urange, vrange)

# makes sets of one non-construction edge end point which are then merged according to each coincident constraint 
def extractcoincidentnodes(cutlinesketch):
	gsegs = dict((i, x.Geometry)  for i, x in enumerate(cutlinesketch.GeometryFacadeList)  if not x.Construction)
	gcpts = [ ((d.First, d.FirstPos), (d.Second, d.SecondPos))  for d in cutlinesketch.Constraints  if d.IsActive and d.Type == "Coincident" and d.First in gsegs and d.Second in gsegs ]
	gcptmap = dict((x, set([x]))  for x in [ (i, 1)  for i in gsegs.keys() ] + [ (i, 2)  for i in gsegs.keys() ] )
	for a, b in gcpts:
		u = gcptmap[a].union(gcptmap[b])
		for c in u:
			gcptmap[c] = u
		assert gcptmap[b] is gcptmap[a]
	gcptsets = [ ]
	gcptnmap = { }
	for x in gcptmap.keys():
		if gcptmap[x]:
			for y in gcptmap[x]:
				assert y not in gcptnmap
				gcptnmap[y] = len(gcptsets)
			gcptsets.append(gcptmap[x].copy())
			gcptmap[x].clear()
	return gcptsets, gcptnmap

# make a sorted array of edges according to angle from each connection list
def coincidentnodetanges(cutlinesketch, gcptset):
	g = cutlinesketch.Geometry
	Dpts = [ (g[i].StartPoint if d == 1 else g[i].EndPoint)  for i, d in gcptset ]
	Dptsavg = sum(Dpts, Vector())*(1.0/len(Dpts))
	assert max([(Dptsavg - x).Length  for x in Dpts]) < 0.001, ("Coincident point not coincident", Dpts)
	Dpts = [ (g[i].StartPoint if d == 1 else g[i].EndPoint)  for i, d in gcptset ]
	gcptorder = [ ]
	for i, d in gcptset:
		v = (g[i].tangent(g[i].FirstParameter)[0] if d == 1 else -g[i].tangent(g[i].LastParameter)[0])
		gcptorder.append((math.atan2(v.y, v.x), (i, d)))
	gcptorder.sort()
	return gcptorder
	
def extractsequences(gcptorders, gcptnmap):
	visited2 = { }
	seqs = [ ]
	for i, d in gcptnmap.keys():
		if (i, d) not in visited2:
			seq = [ ]
			while (i, d) not in visited2:
				seq.append((i, d))
				visited2[(i, d)] = len(seqs)
				dE = (2 if d == 1 else 1)
				gcptn = gcptorders[gcptnmap[(i, dE)]]
				for j in range(len(gcptn)):
					if gcptn[j][1] == (i, dE):
						i2, d2 = gcptn[(j+len(gcptn)-1)%len(gcptn)][1]
						break
				else:
					assert False, "Failed to find our edge in node"
				i, d = i2, d2
			seqs.append(seq)
	return seqs

def sequencetopoints(cutlinesketch, seq, legsampleleng):
	points = [ ]
	for i, d in seq:
		l = cutlinesketch.Geometry[i]
		num = int(math.ceil(l.length()/legsampleleng) + 1)
		params = numpy.linspace(l.FirstParameter, l.LastParameter, num)
		if d == 2:
			params = list(reversed(params))
		for a in params[:-1]:
			points.append(l.value(a))
	return points
	
def orientationclockwise(points):
	jbl, ptbl = min(enumerate(points), key=lambda X:(X[1].y, X[1].x))
	jblp1 = (jbl+1)%len(points)
	jblp2 = (jbl+2)%len(points)
	jblm1 = (jbl+len(points)-1)%len(points)
	jblm2 = (jbl+len(points)-2)%len(points)
	ptblFore = points[jblp1]
	ptblBack = points[jblm1]
	vFore = ptblFore - ptbl
	vBack = ptblBack - ptbl
	#print (ptblBack, ptbl, ptblFore, jbl, jblp1, jblm1, points[jblp2], points[jblm2])
	return (math.atan2(vFore.x, vFore.y) < math.atan2(vBack.x, vBack.y))
	
if R13type:
	patchnamelookups = {
	'TSR':(2.1, 0.5),
	'TSF':(2.1,1),
	'LE1':(1, 1.2),
	'LE2':(2.8, 1.2),
	'LE3':(3.9, 1.2),
	'US':(2.2, 1.7)
	}
else:
	patchnamelookups = {   
	'US1':(0.851, 2),
	'US2':(3.907, 2),
	'LEI1':(0.207, 1.4),
	'LEI2':(0.622, 1.4),
	'LEI3':(1.045, 1.4),
	'LEI4':(1.481, 1.4),
	'LEI5':(2.1, 1.4),
	'LEI6':(3, 1.4),
	'LEI7':(4, 1.4),
	'LEI8':(5.2, 1.4),
	'TSF1':(0.851, 1),
	'TSF2':(3.8, 1),
	'TSM1':(0.415, 0.8),
	'TSM2':(1.266, 0.8),
	'TSM3':(3.394, 0.8),
	'TSR':(4.082, 0) }

def getnameofpolygon(points):
	avgpt = sum(points, Vector())*(1.0/len(points))
	closestname = min(((avgpt - Vector(p[0]*1000, p[1]*1000)).Length, name)  for name, p in patchnamelookups.items())[1]
	return closestname

clw = getemptyfolder(doc, "UVPolygons")


# find the sets of nodes from the coincident constraints
gcptsets, gcptnmap = extractcoincidentnodes(cutlinesketch)
gcptorders = [ coincidentnodetanges(cutlinesketch, gcptset)  for gcptset in gcptsets ]

# derive the sequences of (edge, startend) for each contour
seqs = extractsequences(gcptorders, gcptnmap)

polyseqs = { }
for n, seq in enumerate(seqs):
	points = sequencetopoints(cutlinesketch, seq, legsampleleng=3.0)
	if not orientationclockwise(points):
		closestname = getnameofpolygon(points)
		for i, d in seq:
			if i not in polyseqs:
				polyseqs[i] = [ ]
			polyseqs[i].append(closestname)
		ws = createobjectingroup(doc, clw, "Part::Feature", "w%s"%(closestname))
		ws.Shape = Part.makePolygon(points+[points[0]])
		lam = n/len(seqs)
		ws.ViewObject.PointColor = (0.5 + lam*0.5, 1.0 - lam*0.5, 0.8)




polylengthsheet = getemptyobject(doc, 'Spreadsheet::Sheet', "polylengths")
polylengthsheet.set("A1", "seqno")
polylengthsheet.set("B1", "poly1")
polylengthsheet.set("C1", "poly2")
polylengthsheet.set("D1", "leng3D")
polylengthsheet.set("E1", "leng_poly1")
polylengthsheet.set("F1", "leng_poly2")
prow = 2
sequvpoints = { }
for i, polys in polyseqs.items():
    if len(polys) == 2:
        l = cutlinesketch.Geometry[i]
        legsampleleng = 3.0
        num = int(math.ceil(l.length()/legsampleleng) + 1)
        params = numpy.linspace(l.FirstParameter, l.LastParameter, num)
        uvpoints = [ l.value(a)  for a in params ]
        sequvpoints[i] = [ P2(p.x, p.y)  for p in uvpoints ]
        t3dpoints = [ seval(p.x, p.y)  for p in uvpoints ]
        t3leng = sum((a-b).Length  for a, b in zip(t3dpoints, t3dpoints[1:]))
        polylengthsheet.set("A%d"%prow, str(i))
        polylengthsheet.set("B%d"%prow, polys[0])
        polylengthsheet.set("C%d"%prow, polys[1])
        polylengthsheet.set("D%d"%prow, str(t3leng))
        prow += 1

#import imp
#import p7modules.p7wingflatten_projfuncs
#imp.reload(p7modules.p7wingflatten_projfuncs)

from p7modules.p7wingflatten_projfuncs import cp2t, cpolyuvvectorstransC, generateTransColumns, findcctriangleinmesh, projectspbarmeshF
uspacing, vspacing = 20, 10
polylengthsheet.recompute()

uvtriangulations = doc.UVTriangulations.OutList if doc.findObjects(Label="UVTriangulations") else []
striangulations = doc.STriangulations.OutList if doc.findObjects(Label="STriangulations") else []
sflattened = doc.SFlattened.OutList if doc.findObjects(Label="SFlattened") else []
for I in range(len(uvtriangulations)):
    uvmesh = uvtriangulations[I]
    surfacemesh = striangulations[I]
    flattenedmesh = sflattened[I]
    assert uvmesh.Mesh.CountFacets == flattenedmesh.Mesh.CountFacets == surfacemesh.Mesh.CountFacets
    name = uvmesh.Name[1:]
    xpart, uvtranslistCcolumns = generateTransColumns(uvmesh, flattenedmesh, urange, vrange)
    print(name)
    for irow in range(2, prow):
        poly1 = polylengthsheet.get("B%d"%irow)
        poly2 = polylengthsheet.get("C%d"%irow)
        if name == poly1 or name == poly2:
            uvpoints = sequvpoints[int(polylengthsheet.get("A%d"%irow))]
            spsJF = [ projectspbarmeshF(sp, xpart, uvtranslistCcolumns)  for sp in uvpoints ]
            flatleng = sum((a-b).Len()  for a, b in zip(spsJF, spsJF[1:]))
            polylengthsheet.set("%s%d"%("E" if name == poly1 else "F", irow), str(flatleng))
