import sys
print(sys.path[-1])

from p7modules.barmesh.basicgeo import P2, P3, Partition1, Along, I1
from p7modules.barmesh import barmesh
from p7modules.barmesh.barmesh import Bar
import numpy

uspacing, vspacing = 20, 20

def cp2t(t):
	return [ P2(t[0][0], t[0][1]), P2(t[1][0], t[1][1]), P2(t[2][0], t[2][1]) ]

def cpolyuvvectorstransC(uvpts, fptsT):
	assert len(uvpts) == len(fptsT)
	n = len(uvpts)
	area = abs(P2.Dot(fptsT[1] - fptsT[0], P2.APerp(fptsT[2] - fptsT[0]))*0.5)
	uvarea = abs(P2.Dot(uvpts[1] - uvpts[0], P2.APerp(uvpts[2] - uvpts[0]))*0.5)
	if uvarea == 0:
		return { "uvarea":0.0 }

	cpt = sum(uvpts, P2(0,0))*(1.0/n)
	cptT = sum(fptsT, P2(0,0))*(1.0/n)
	jp = max((abs(P2.Dot(uvpts[j] - cpt, P2.APerp(uvpts[(j+1)%n] - cpt))), j)  for j in range(n))
	vj = uvpts[jp[1]] - cpt
	vj1 = uvpts[(jp[1]+1)%n] - cpt

	urvec = P2(vj1.v, -vj.v)
	#if P2.Dot(urvec, P2(vj.u, vj1.u)) == 0:
	#	print(jp[1], uvpts, uvarea)
	urvec = urvec*(1.0/P2.Dot(urvec, P2(vj.u, vj1.u)))
	vrvec = P2(vj1.u, -vj.u)
	vrvec = vrvec*(1.0/P2.Dot(vrvec, P2(vj.v, vj1.v)))
	# this has gotten muddled.  Should be simpler since the following two are negative of each other
	# P2.Dot(urvec, P2(vj.u, vj1.u)) = vj1.v*vj.u - vj.v*vj1.u
	# P2.Dot(vrvec, P2(vj.v, vj1.v)) = vj1.u*vj.v - vj.u*vj1.v
	# set solve: (urvec.u*vj + urvec.v*vj1).v = 0, which is why it uses only v components 

	vjT = fptsT[jp[1]] - cptT
	vj1T = fptsT[(jp[1]+1)%n] - cptT

	# vc = p - cc["cpt"]
	#vcp = cc["urvec"]*vc.u + cc["vrvec"]*vc.v
	#vcs = cc["vj"]*vcp.u + cc["vj1"]*vcp.v ->  vc

	return { "cpt":cpt, "cptT":cptT, "urvec":urvec, "vrvec":vrvec, 
			 "vj":vj, "vj1":vj1, "vjT":vjT, "vj1T":vj1T, "area":area, "uvarea":uvarea }



def generateTransColumns(uvmesh, flattenedmesh, urange, vrange):
	uvtranslist = [ cpolyuvvectorstransC(cp2t(a.Points), cp2t(b.Points))  for a, b in zip(uvmesh.Mesh.Facets, flattenedmesh.Mesh.Facets) ]
	uvareacutoff = max(t["uvarea"]  for t in uvtranslist)*0.2
	uvtranslistC = [t  for t in uvtranslist  if t["uvarea"] > uvareacutoff]
	print("discarding", len(uvtranslist)-len(uvtranslistC), "small triangles out of", len(uvtranslist))

	# recreate the original partition of the urange which matches the zoning of the areas already there
	# (we could derive it from the inputs, but I'm porting this code across from functions which 
	# depended on carrying across the same barmesh as between the operations, instead of saving 
	# them out to unstructured meshes as we have here now) 
	radoffset = 6
	rd2 = max(uspacing, vspacing, radoffset*2) + 10
	urgA, vrgA = I1(*urange).Inflate(60), I1(*vrange).Inflate(110)
	xpartA = Partition1(urgA.lo, urgA.hi, int(urgA.Leng()/uspacing + 2))
	ypartA = Partition1(vrgA.lo, vrgA.hi, int(vrgA.Leng()/vspacing + 2))
	uvtranslistCcolumns = [ [ ]  for ix in range(xpartA.nparts) ] 
	for uvtrans in uvtranslistC:
		uvtranslistCcolumns[xpartA.GetPart(uvtrans["cpt"].u)].append(uvtrans)
	return xpartA, uvtranslistCcolumns

def findcctriangleinmesh(sp, xpart, uvtranslistCcolumns):
	if not (xpart.lo < sp[0] < xpart.hi):
		return None
	ix = xpart.GetPart(sp[0])
	if len(uvtranslistCcolumns[ix]) == 0:
		return None
	return min((cc  for cc in uvtranslistCcolumns[ix]), key=lambda X: (X["cpt"] - sp).Len())

def projectspbarmeshF(sp, xpart, uvtranslistCcolumns, bFlattenedPatches=True):
	cc = findcctriangleinmesh(sp, xpart, uvtranslistCcolumns)
	if cc is None:
		return None
	if abs(cc["cpt"][0] - sp.u) > uspacing or abs(cc["cpt"][1] - sp.v) > vspacing:
		return None
	vc = sp - cc["cpt"]
	vcp = cc["urvec"]*vc.u + cc["vrvec"]*vc.v
	vcs = cc["vj"]*vcp.u + cc["vj1"]*vcp.v # should be same as vc
	vcsT = cc["vjT"]*vcp.u + cc["vj1T"]*vcp.v
	if bFlattenedPatches:
		return vcsT + cc["cptT"]
	return vcs + cc["cpt"]


