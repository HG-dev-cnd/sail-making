# -*- coding: utf-8 -*-
# Module to read the sections from SectionGroup and define the seval(u, v) parametric function on this surface

import Draft, Part, Mesh, Sketcher
import DraftGeomUtils
import math, os, csv
import numpy
from FreeCAD import Vector, Rotation
import FreeCAD

def removeObjectRecurse(doc, objname):
	for o in doc.findObjects(Name=objname)[0].OutList:
		removeObjectRecurse(doc, o.Name)
	doc.removeObject(objname)
	
def getemptyobject(doc, objtype, objname):
	if doc.findObjects(Name=objname):
		removeObjectRecurse(doc, objname)
		doc.recompute()
	return doc.addObject(objtype, objname)

def createobjectingroup(doc, group, objtype, objname):
	if group == None:
		return getemptyobject(doc, objtype, objname)
	obj = doc.addObject(objtype, objname)
	obj.adjustRelativeLinks(group)
	group.addObject(obj)
	return obj

def makeclearedsketch(doc, lname):
	objs = doc.findObjects(Label=lname)
	obj = objs[0] if objs else doc.addObject("Sketcher::SketchObject", lname)
	obj.Label = lname
	obj.deleteAllGeometry()
	return obj

def getemptyfolder(doc, foldername):
	objs = doc.findObjects(Label=foldername)
	folder = objs[0] if objs else doc.addObject("App::DocumentObjectGroup", foldername)
	for o in folder.OutList:
		removeObjectRecurse(doc, o.Name)
	return folder


def paramintconv(u, uvals):
	j0, j1 = 0, len(uvals)-1
	while j1 - j0 >= 2:
		j = (j1 + j0)//2
		if u <= uvals[j]:
			j1 = j
		else:
			j0 = j
	return j0 + (u-uvals[j0])/(uvals[j1]-uvals[j0])

def uvrectangle(urange, vrange, sketchname,doc):   # to abolish
	sketch = getemptyobject(doc, "Sketcher::SketchObject", sketchname)

	p00 = Vector(urange[0], vrange[0])
	p01 = Vector(urange[0], vrange[1])
	p10 = Vector(urange[1], vrange[0])
	p11 = Vector(urange[1], vrange[1])

	e0x = sketch.addGeometry(Part.LineSegment(p00, p01), True)
	e1x = sketch.addGeometry(Part.LineSegment(p10, p11), True)
	ex0 = sketch.addGeometry(Part.LineSegment(p00, p10), True)
	ex1 = sketch.addGeometry(Part.LineSegment(p01, p11), True)

	sketch.addConstraint(Sketcher.Constraint("Vertical", e0x))
	sketch.addConstraint(Sketcher.Constraint("Vertical", e1x))
	sketch.addConstraint(Sketcher.Constraint("Horizontal", ex0))
	sketch.addConstraint(Sketcher.Constraint("Horizontal", ex1))

	sketch.addConstraint(Sketcher.Constraint("Coincident", e0x, 1, ex0, 1))
	sketch.addConstraint(Sketcher.Constraint("Coincident", e0x, 2, ex1, 1))
	sketch.addConstraint(Sketcher.Constraint("Coincident", ex0, 2, e1x, 1))
	sketch.addConstraint(Sketcher.Constraint("Coincident", e1x, 2, ex1, 2))

	sketch.addConstraint(Sketcher.Constraint('DistanceX', e0x, 1, urange[0])) 
	sketch.addConstraint(Sketcher.Constraint('DistanceY', e0x, 1, vrange[0])) 
	sketch.addConstraint(Sketcher.Constraint('DistanceX', e1x, 2, urange[1])) 
	sketch.addConstraint(Sketcher.Constraint('DistanceY', e1x, 2, vrange[1])) 

	for i in range(len(sketch.Constraints)):
		sketch.setVirtualSpace(i, True)
	return sketch


Vparameter_wingsectionL = "Vparameter_wingsect"
class WingEval:
	def __init__(self, doc, R13type=True):

		Vparameter_wingsection = doc.findObjects(Label=Vparameter_wingsectionL)[0]

		if not isinstance(doc, FreeCAD.Document):
			print("Need to pass in document as first parameter, in which look for and filter out the WingSections list") 
		wingsectionsgroup = doc.findObjects(Label="WingSections")[0]
		self.sections = [ ]
		Vparameter_wingsection = None
		for lsection in wingsectionsgroup.OutList:
			if lsection.Label.startswith("wingsection_"):
				self.sections.append(lsection)
			if lsection.Label == Vparameter_wingsectionL:
				Vparameter_wingsection = lsection
		print("Found", self.sections)
		assert Vparameter_wingsection != None, "Failed to find Vparameter_wingsect"
		
		self.R13type = R13type
		
		self.Isection = 5
		self.sectionpoints = [ ]
		for section in self.sections:
			self.sectionpoints.append([ v.Point  for v in section.Shape.OrderedVertexes ])

		Vparameter_wingsectionpoints = [ v.Point  for v in Vparameter_wingsection.Shape.OrderedVertexes ]
		self.Ichordlengths = [ 0 ]
		for p0, p1 in zip(Vparameter_wingsectionpoints, Vparameter_wingsectionpoints[1:]):
			self.Ichordlengths.append(self.Ichordlengths[-1] + (p0-p1).Length)
			
		self.vrange = [ self.Ichordlengths[0], self.Ichordlengths[-1] ]

		self.xvals = [ spoints[0].x  for spoints in self.sectionpoints ]  # sections assumed to lie in constant x planes
		self.uvals = self.xvals
		self.urange = [0, self.uvals[-1]]
		
		self.leadingedgesV = [ ]
		for spoints in self.sectionpoints:
			j = max(range(len(spoints)), key=lambda jj:spoints[jj].y)
			self.leadingedgesV.append(self.Ichordlengths[j])

		print("Ranges", self.urange, self.vrange)
	
	def sueval(self, i, v):
		vc = paramintconv(v, self.Ichordlengths)
		j = max(0, min(len(self.Ichordlengths)-2, int(vc)))
		m = vc - j
		return self.sectionpoints[i][j]*(1-m) + self.sectionpoints[i][j+1]*m

	def seval(self, u, v):
		uc = paramintconv(u, self.uvals)
		i = max(0, min(len(self.uvals)-2, int(uc)))
		m = uc - i
		p0 = self.sueval(i, v)
		p1 = self.sueval(i+1, v)
		return p0*(1-m) + p1*m

	def inverse_seval(self, x, y, bupperface, tol=0.001):
		xc = paramintconv(x, self.xvals)
		i = max(0, min(len(self.xvals)-2, int(xc)))
		m = xc - i
		u = self.uvals[i]*(1-m) + self.uvals[i+1]*m
		vlo, vhi = (self.vrange[0] if bupperface else self.vrange[1]), self.leadingedgesV[0]

		plo0 = self.sueval(i, vlo)
		plo1 = self.sueval(i+1, vlo)
		plo = plo0*(1-m) + plo1*m

		phi0 = self.sueval(i, vhi)
		phi1 = self.sueval(i+1, vhi)

		phi = phi0*(1-m) + phi1*m
		if not (plo.y - tol*5 <= y <= phi.y + tol*5):
			print("** Warning %f,%f out of range as not %f < %f < %f" % (x, y, plo.y, y, phi.y))
		while abs(phi.y - plo.y) > tol:
			vmid = (vlo + vhi)/2
			pmid0 = self.sueval(i, vmid)
			pmid1 = self.sueval(i+1, vmid)
				
			pmid = pmid0*(1-m) + pmid1*m
			if pmid.y < y:
				vlo, plo0, plo1, plo = vmid, pmid0, pmid1, pmid
			else:
				vhi, phi0, phi1, phi = vmid, pmid0, pmid1, pmid
		return u, vlo
